package br.com.orsegups;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
public class ChipControlV2Application extends SpringBootServletInitializer {

    private static final Logger log = LoggerFactory.getLogger(ChipControlV2Application.class);

    public static void main(String[] args) throws UnknownHostException {
        Environment env = SpringApplication.run(ChipControlV2Application.class, args).getEnvironment();
        log.info(
            "\n----------------------------------------------------------\n\t" +
                    "Application '{}' is running! Access URLs:\n\t" +
                    "Local: \t\thttp://{}:{}\n\t" +
                    "External: \thttp://{}:{}" +
            "\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            InetAddress.getLocalHost().getHostName(),
            env.getProperty("server.port"),
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port")
        );

    }

}

