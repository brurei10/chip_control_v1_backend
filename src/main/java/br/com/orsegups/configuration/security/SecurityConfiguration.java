package br.com.orsegups.configuration.security;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.inject.Inject;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Inject
    private Http401UnauthorizedEntryPoint authenticationEntryPoint;

    @Inject
    private UserDetailsService userDetailsService;

    @Inject
    private TokenProvider tokenProvider;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Inject
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        try {
            auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .accessDeniedHandler(accessDeniedHandler())
            .and()
                .authorizeRequests()
                // Chip Controller
                .antMatchers("/api/chip/createInBatch").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chip/getByIccId").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/findByTechnician").hasAuthority(RolesConstants.TECHNICIAN)
                .antMatchers("/api/chip/findByRegional").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/findInStock").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/findInTechnician").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/findInCustomer").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/requestCancellation").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/findToCancel").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chip/cancel/{id}").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chip/findInEquipment").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/findUsage").hasAuthority(RolesConstants.TECHNICIAN)
                .antMatchers("/api/chip/unlinkTechnician/{id}").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chip/findCanceled").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chip/undoCancellation").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chip/cancelInBatch").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chip/findChipsFromExcel").hasAuthority(RolesConstants.ADMIN)
                // ChipLot Controller
                .antMatchers("/api/chipLot/create").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chipLot/get/{chipLotId}").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chipLot/listToReceive").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chipLot/listSent").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/chipLot/receive").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chipLot/cancel/{chipLotId}").hasAuthority(RolesConstants.ADMIN) //, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/chipLot/countWaitingByRegional/{regionalId}").hasAuthority(RolesConstants.ADMIN)
                // Company Controller
                .antMatchers("/api/company/list").hasAuthority(RolesConstants.ADMIN)
                // CustomerLink Controller
                .antMatchers("/api/customerLink/create").hasAnyAuthority(RolesConstants.REGIONAL_MANAGER, RolesConstants.TECHNICIAN)
                .antMatchers("/api/customerLink/listInstalled").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/customerLink/cancel").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                // Operator Controller
                .antMatchers("/api/operator/list").hasAuthority(RolesConstants.ADMIN)
                // RegionalOffice Controller
                .antMatchers("/api/regionalOffice/list").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/regionalOffice/getByAcronym").authenticated()
                // ServiceOrder Controller
                .antMatchers("/api/serviceOrder/list").hasAnyAuthority(RolesConstants.REGIONAL_MANAGER, RolesConstants.TECHNICIAN)
                // Technician Controller
                .antMatchers("/api/technician/list").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/technician/chipAmount/{technicianId}").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                // TechnicianDelivery Controller
                .antMatchers("/api/technicianDelivery/create").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                // PrimeAlarm Controller
                .antMatchers("/api/primeAlarm/linkCustomerChip").hasAuthority(RolesConstants.VETTI)
                // Dashboard Controller
                .antMatchers("/api/dashboard/stats").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/dashboard/stockStats").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/dashboard/operatorStats").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/dashboard/officeStats").hasAuthority(RolesConstants.ADMIN)
                .antMatchers("/api/dashboard/equipmentStats").hasAuthority(RolesConstants.ADMIN)
                // EquipmentLink Controller
                .antMatchers("/api/equipmentLink/create").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/equipmentLink/listLinked").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                .antMatchers("/api/equipmentLink/cancel").hasAuthority(RolesConstants.REGIONAL_MANAGER)
                // ChipHistory Controller
                .antMatchers("/api/chipHistory/getByChip/{chipId}").hasAnyAuthority(RolesConstants.ADMIN, RolesConstants.REGIONAL_MANAGER)
                // Report Controller
                .antMatchers("/api/report/inCancellation").hasAuthority(RolesConstants.ADMIN)
                // Any Unmapped EndPoint
                .antMatchers("/api/**").authenticated()
            .and()
            .csrf()
                .disable()
                .headers()
                .frameOptions().disable()
            .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .apply(securityConfigurerAdapter());
    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/app/**/*.{js,html}")
            .antMatchers("/bower_components/**")
            .antMatchers("/i18n/**")
            .antMatchers("/content/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }
}
