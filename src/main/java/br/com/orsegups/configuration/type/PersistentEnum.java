package br.com.orsegups.configuration.type;

public interface PersistentEnum {
    Long getCode();
}
