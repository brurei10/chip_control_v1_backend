package br.com.orsegups.controller.chip;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.configuration.locale.Messages;
import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.dto.TableOptionDTO;
import br.com.orsegups.entity.*;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.service.*;
import br.com.orsegups.service.utils.CancelUtilService;
import br.com.orsegups.util.SessionUtil;
import br.com.orsegups.validator.ChipValidator;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class ChipFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private CompanyService companyService;
    @Inject
    private OperatorService operatorService;
    @Inject
    private RegionalOfficeService regionalOfficeService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private TechnicianDeliveryChipService technicianDeliveryChipService;
    @Inject
    private CancelUtilService cancelUtilService;
    @Inject
    private SessionUtil sessionUtil;
    @Inject
    private ChipValidator chipValidator;
    @Inject
    private Messages messages;

    public List<ChipDTO> createInBatch(ChipVMI vmi) {
        chipValidator.validateCreate(vmi);

        Company company = companyService.getFromDTO(vmi.getCompany());
        Operator operator = operatorService.getFromDTO(vmi.getOperator());
        RegionalOffice regionalOffice = regionalOfficeService.findStockTI();

        List<Chip> chips = chipService.createAll(vmi.getChips(), company, operator, regionalOffice);
        chipHistoryService.chipCreated(chips);

        return chips.stream().map(ChipDTO::new).collect(Collectors.toList());
    }

    public ChipDTO findByIccId(String iccId, Boolean checkRegional, Boolean checkNotInLot) {
        Chip chip = chipService.findByIccId(iccId);
        if (checkRegional)
            chipValidator.validateChipInRegionalStock(chip, sessionUtil.getCurrentUserRegional());
        if (checkNotInLot)
            chipValidator.validateChipNotInOpenLot(chip);

        return new ChipDTO(chip);
    }

    public List<ChipDTO> findByTechnician() {
        return chipService.findByTechnicianId(sessionUtil.getCurrentUserId())
                .stream().map(ChipDTO::new).collect(Collectors.toList());
    }

    public List<ChipDTO> findByRegional(Long regionalId) {
        if (sessionUtil.isCurrentUserAdmin()) {
            return chipService.findByRegional(regionalId)
                    .stream().map(ChipDTO::new).collect(Collectors.toList());
        } else {
            return chipService.findByRegional(sessionUtil.getCurrentUserRegional())
                    .stream().map(ChipDTO::new).collect(Collectors.toList());
        }
    }

    public ChipVMO findInStock(ChipVMI vmi) {
        ChipVMO vmo = new ChipVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("c.created_date"), Sort.Order.desc("c.icc_id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage()-1, tableOption.getItemPerPage(), sort);
        List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<Chip> page = chipService.findInStockByFilters(vmi.getSearchText(), regionalIds, pg);

        vmo.setChips(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;
    }

    public ChipVMO findInTechnician(ChipVMI vmi) {
        ChipVMO vmo = new ChipVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("td.delivery_date"), Sort.Order.desc("c.icc_id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage()-1, tableOption.getItemPerPage(), sort);
        List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<Chip> page = chipService.findInTechnicianByFilters(vmi.getSearchText(), regionalIds, pg);

        vmo.setChips(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;

    }

    public ChipVMO findInCustomer(ChipVMI vmi) {
        ChipVMO vmo = new ChipVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("cl.installation_date"), Sort.Order.desc("c.icc_id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage()-1, tableOption.getItemPerPage(), sort);
        List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<Chip> page = chipService.findInCustomerByFilters(vmi.getSearchText(), regionalIds, pg);

        vmo.setChips(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;

    }

    public ChipVMO findInEquipment(ChipVMI vmi) {
        ChipVMO vmo = new ChipVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("el.installation_date"), Sort.Order.desc("c.icc_id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage()-1, tableOption.getItemPerPage(), sort);
        List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<Chip> page = chipService.findInEquipmentByFilters(vmi.getSearchText(), regionalIds, pg);

        vmo.setChips(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;

    }

    public ChipDTO requestCancellation(ChipVMI vmi) {
        Chip chip = chipService.findById(vmi.getId());
        return new ChipDTO(cancelUtilService.requestChipCancellation(chip, vmi.getReason()));
    }

    public ChipDTO cancel(Long id) {
        Chip chip = chipService.findById(id);
        chip = cancelUtilService.cancel(chip);
        return new ChipDTO(chip);
    }

    public List<ChipDTO> cancelInBatch(ChipVMI vmi) {
        List<Long> chipIds = vmi.getChips().stream().map(ChipDTO::getId).collect(Collectors.toList());
        List<Chip> chips = chipService.findAllById(chipIds);
        chips = cancelUtilService.cancel(chips);
        return chips.stream().map(ChipDTO::new).collect(Collectors.toList());
    }
    public List<ChipDTO> cancellationInBatch(ChipVMI vmi) {
        List<Long> chipIds = vmi.getChips().stream().map(ChipDTO::getId).collect(Collectors.toList());
        List<Chip> chips = chipService.findAllById(chipIds);
        chips = cancelUtilService.requestChipCancellation(chips, "Cancelado em lote pela tela de cancelamento");
        return chips.stream().map(ChipDTO::new).collect(Collectors.toList());
    }

    public ChipVMO findToCancel(ChipVMI vmi) {
        ChipVMO vmo = new ChipVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("c.requested_cancellation_date"), Sort.Order.desc("c.icc_id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage()-1, tableOption.getItemPerPage(), sort);
        List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<Chip> page = chipService.findToCancelByFilters(vmi.getSearchText(), regionalIds, pg);

        vmo.setChips(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;
    }

    public ChipDTO findUsage(String iccId) {
        return new ChipDTO(chipService.findByIccId(iccId));
    }

    public ChipDTO unlinkTechnician(Long id) {
        Chip chip = chipService.findById(id);
        if (!sessionUtil.isCurrentUserAdmin())
            chipValidator.validateChipInRegional(chip, sessionUtil.getCurrentUserRegional());

        TechnicianDeliveryChip technicianDeliveryChip = technicianDeliveryChipService.findByChipAndTechnicianDelivery(chip, chip.getTechnicianDelivery());
        cancelUtilService.cancelTechnicianDeliveryChip(technicianDeliveryChip);
        chip.setTechnicianDelivery(null);
        chip = chipService.save(chip);

        return new ChipDTO(chip);
    }

    public ChipVMO findCanceled(ChipVMI vmi) {
        ChipVMO vmo = new ChipVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("c.cancellation_date"), Sort.Order.desc("c.icc_id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage()-1, tableOption.getItemPerPage(), sort);
        List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<Chip> page = chipService.findCanceledByFilters(vmi.getSearchText(), regionalIds, pg);

        vmo.setChips(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;
    }

    public ChipDTO undoCancellation(ChipVMI vmi) {
        Chip chip = chipService.findById(vmi.getId());
        return new ChipDTO(cancelUtilService.undoCancellation(chip, vmi.getReason()));
    }

    public List<ChipDTO> findChipsFromExcel(MultipartFile multipartFile) {
        File tempFile = null;
        try {
            tempFile = File.createTempFile("tmp_file", multipartFile.getOriginalFilename(), null);
            tempFile.deleteOnExit();
            multipartFile.transferTo(tempFile);

            Workbook workbook = WorkbookFactory.create(tempFile);
            Sheet sheet = workbook.getSheetAt(0);
            DataFormatter dataFormatter = new DataFormatter();
            List<String> iccIds = new ArrayList<>();
            sheet.forEach(row -> iccIds.add(dataFormatter.formatCellValue(row.getCell(0)).trim()));
            return chipService.findByIccIds(iccIds).stream().map(ChipDTO::new).collect(Collectors.toList());
        } catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
            e.printStackTrace();
            throw new CustomException(messages.get("excel.export.exception"));
        } finally {
            if (tempFile != null)
                tempFile.delete();
        }
    }
}
