package br.com.orsegups.controller.chip;

import br.com.orsegups.dto.*;

import java.util.List;

public class ChipVMI {

    private List<ChipDTO> chips;
    private CompanyDTO company;
    private OperatorDTO operator;

    private String searchText;
    private TableOptionDTO tableOption;
    private RegionalOfficeDTO regional;

    private Long id;
    private String reason;

    public List<ChipDTO> getChips() {
        return chips;
    }

    public void setChips(List<ChipDTO> chips) {
        this.chips = chips;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public OperatorDTO getOperator() {
        return operator;
    }

    public void setOperator(OperatorDTO operator) {
        this.operator = operator;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public RegionalOfficeDTO getRegional() {
        return regional;
    }

    public void setRegional(RegionalOfficeDTO regional) {
        this.regional = regional;
    }
}
