package br.com.orsegups.controller.chip;

import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.TableOptionDTO;
import br.com.orsegups.entity.Chip;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChipVMO {

    private List<ChipDTO> chips;
    private TableOptionDTO tableOption;

    public ChipVMO() {
        super();
    }

    public List<ChipDTO> getChips() {
        return chips;
    }

    public void setChipDTOs(List<ChipDTO> chips) {
        this.chips = chips;
    }

    public void setChips(List<Chip> chipLots) {
        this.chips = chipLots.stream().map(ChipDTO::new).collect(Collectors.toList());
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }
}
