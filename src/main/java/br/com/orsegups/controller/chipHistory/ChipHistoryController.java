package br.com.orsegups.controller.chipHistory;

import br.com.orsegups.configuration.annotation.ChipControlController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.inject.Inject;

@ChipControlController("/api/chipHistory")
public class ChipHistoryController {

    @Inject
    private ChipHistoryFacade chipHistoryFacade;

    @GetMapping(value = "/getByChip/{chipId}")
    public ResponseEntity<ChipHistoryVMO> getHistory(@PathVariable("chipId") Long chipId) {
        return new ResponseEntity<>(chipHistoryFacade.getHistory(chipId), HttpStatus.OK);
    }

}
