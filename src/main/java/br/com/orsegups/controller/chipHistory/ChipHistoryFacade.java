package br.com.orsegups.controller.chipHistory;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.ChipHistory;
import br.com.orsegups.service.*;
import br.com.orsegups.util.SessionUtil;
import br.com.orsegups.validator.ChipValidator;

import javax.inject.Inject;
import java.util.List;

@ChipControlFacade
public class ChipHistoryFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private SessionUtil sessionUtil;
    @Inject
    private ChipValidator chipValidator;


    public ChipHistoryVMO getHistory(Long chipId) {
        Chip chip = chipService.findById(chipId);
        if (!sessionUtil.isCurrentUserAdmin())
            chipValidator.validateChipInRegional(chip, sessionUtil.getCurrentUserRegional());

        List<ChipHistory> chipHistories = chipHistoryService.findByChipId(chipId);

        return new ChipHistoryVMO(chip, chipHistories);
    }
}
