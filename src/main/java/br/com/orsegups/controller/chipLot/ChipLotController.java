package br.com.orsegups.controller.chipLot;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.ChipLotDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@ChipControlController("/api/chipLot")
public class ChipLotController {

    @Inject
    private ChipLotFacade chipLotFacade;

    @PostMapping(value = "/create")
    public ResponseEntity<ChipLotDTO> create(@RequestBody ChipLotVMI vmi) {
        return new ResponseEntity<>(chipLotFacade.create(vmi), HttpStatus.CREATED);
    }

    @GetMapping(value = "/get/{chipLotId}")
    public ResponseEntity<ChipLotDTO> get(@PathVariable("chipLotId") Long chipLotId) {
        return new ResponseEntity<>(chipLotFacade.getById(chipLotId), HttpStatus.OK);
    }

    @PostMapping(value = "/listToReceive")
    public ResponseEntity<ChipLotVMO> listToReceive(@RequestBody ChipLotVMI vmi) {
        return new ResponseEntity<>(chipLotFacade.listToReceive(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/listSent")
    public ResponseEntity<ChipLotVMO> listSent(@RequestBody ChipLotVMI vmi) {
        return new ResponseEntity<>(chipLotFacade.listSent(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/receive")
    public ResponseEntity<ChipLotDTO> receive(@RequestBody ChipLotDTO chipLot) {
        return new ResponseEntity<>(chipLotFacade.receive(chipLot), HttpStatus.OK);
    }

    @DeleteMapping(value = "/cancel/{chipLotId}")
    public ResponseEntity<ChipLotDTO> cancel(@PathVariable("chipLotId") Long chipLotId) {
        return new ResponseEntity<>(chipLotFacade.cancel(chipLotId), HttpStatus.OK);
    }

    @GetMapping(value = "/countWaitingByRegional/{regionalId}")
    public ResponseEntity<Integer> countWaitingByRegional(@PathVariable("regionalId") Long regionalId) {
        return new ResponseEntity<>(chipLotFacade.countWaitingByRegional(regionalId), HttpStatus.OK);
    }

}
