package br.com.orsegups.controller.chipLot;

import br.com.orsegups.dto.ChipLotDTO;
import br.com.orsegups.dto.TableOptionDTO;
import br.com.orsegups.entity.ChipLot;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChipLotVMO {

    private List<ChipLotDTO> chipLots;
    private TableOptionDTO tableOption;

    public List<ChipLotDTO> getChipLots() {
        return chipLots;
    }

    public void setChipLots(List<ChipLot> chipLots) {
        this.chipLots = chipLots.stream().map(ChipLotDTO::new).collect(Collectors.toList());
    }

    public void setChipLotDTOs(List<ChipLotDTO> chipLots) {
        this.chipLots = chipLots;
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }
}
