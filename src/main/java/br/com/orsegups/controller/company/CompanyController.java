package br.com.orsegups.controller.company;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.CompanyDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import javax.inject.Inject;
import java.util.List;


@ChipControlController("/api/company")
public class CompanyController {

    @Inject
    private CompanyFacade companyFacade;

    @GetMapping(value = "/list")
    public ResponseEntity<List<CompanyDTO>> getAll() {
        return new ResponseEntity<>(companyFacade.getAll(), HttpStatus.OK);
    }

}
