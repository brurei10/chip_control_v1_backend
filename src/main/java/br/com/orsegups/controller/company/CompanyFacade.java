package br.com.orsegups.controller.company;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.service.CompanyService;
import br.com.orsegups.dto.CompanyDTO;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class CompanyFacade {

    @Inject
    private CompanyService companyService;

    public List<CompanyDTO> getAll() {
        return companyService.findAll().stream().map(CompanyDTO::new)
                .sorted(Comparator.comparing(CompanyDTO::getName)).collect(Collectors.toList());
    }
}
