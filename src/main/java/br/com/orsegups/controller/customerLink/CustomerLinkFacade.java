package br.com.orsegups.controller.customerLink;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.configuration.locale.Messages;
import br.com.orsegups.dto.CustomerLinkDTO;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.dto.TableOptionDTO;
import br.com.orsegups.dto.TechnicianDTO;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.CustomerLink;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.service.ChipHistoryService;
import br.com.orsegups.service.ChipService;
import br.com.orsegups.service.CustomerLinkService;
import br.com.orsegups.service.utils.CancelUtilService;
import br.com.orsegups.util.SessionUtil;
import br.com.orsegups.validator.ChipValidator;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.inject.Inject;
import java.util.List;

@ChipControlFacade
public class CustomerLinkFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private CustomerLinkService customerLinkService;
    @Inject
    private ChipValidator chipValidator;
    @Inject
    private CancelUtilService cancelUtilService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private SessionUtil sessionUtil;
    @Inject
    private Messages messages;

    public CustomerLinkVMO create(CustomerLinkVMI vmi) {
        Chip chip = chipService.findById(vmi.getChip().getId());
        if (sessionUtil.isCurrentUserTechnician()) {
            chipValidator.validateChipWithTechnician(chip, sessionUtil.getCurrentUserId());

            List<CustomerLink> activeLinks = customerLinkService.findByRemovalDateIsNullAndCompanyIdAndCentralId(
                    vmi.getServiceOrder().getCompanyId(), vmi.getServiceOrder().getCentralId());
            if (!removeLink(vmi, activeLinks)) {
                return new CustomerLinkVMO(activeLinks);
            }
        } else {
            chipValidator.validateChipInRegionalStockOrTechnician(chip, sessionUtil.getCurrentUserRegional());
            vmi.setTechnician(new TechnicianDTO());
        }

        CustomerLink customerLink = getEntityFromVMI(vmi, chip);
        chip.setCustomerLink(customerLink);
        chipService.save(chip);
        customerLink = customerLinkService.save(customerLink);

        chipHistoryService.customerLink(customerLink);
        return new CustomerLinkVMO();
    }

    public Boolean removeLink(CustomerLinkVMI vmi, List<CustomerLink> activeLinks) {
        if (activeLinks.isEmpty()) return true;

        String cancelReason = messages.get("chipCancel.byTechnician.reason", sessionUtil.getCurrentUserLogin());
        if (activeLinks.size() > 1) {
            if (vmi.getCustomerId() != null) {
                CustomerLink linkToCancel = activeLinks.stream().filter(customerLink -> customerLink.getId().equals(vmi.getCustomerId())).findFirst()
                        .orElseThrow(() -> new CustomException(messages.get("chipCancel.byTechnician.notFound")));

                cancelUtilService.requestChipCancellation(linkToCancel.getChip(), cancelReason);
                return true;
            }
        } else {
            if (vmi.getConfirmReplacement() != null && vmi.getConfirmReplacement()) {
                cancelUtilService.requestChipCancellationByCustomer(activeLinks, cancelReason);
                return true;
            }
        }
        return false;

    }

    public CustomerLinkVMO listInstalled(CustomerLinkVMI vmi) {
        CustomerLinkVMO vmo = new CustomerLinkVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("cl.installation_date"), Sort.Order.desc("cl.id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage()-1, tableOption.getItemPerPage(), sort);
        PageDTO<CustomerLink> page = customerLinkService.findInstalledByFilters(vmi.getSearchText(), sessionUtil.getCurrentUserRegionalIds(), pg);

        vmo.setCustomerLinks(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;
    }


    public CustomerLinkDTO cancel(CustomerLinkVMI vmi) {
        CustomerLink customerLink = customerLinkService.findById(vmi.getCustomerId());
        cancelUtilService.requestChipCancellation(customerLink.getChip(), vmi.getReason());
        return new CustomerLinkDTO(customerLink);
    }

    private CustomerLink getEntityFromVMI(CustomerLinkVMI vmi, Chip chip) {
        return new CustomerLink(
                vmi.getTechnician().getId(),
                vmi.getTechnician().getName(),
                vmi.getServiceOrder().getId(),
                vmi.getServiceOrder().getCompanyId(),
                vmi.getServiceOrder().getCentralId(),
                vmi.getServiceOrder().getCentralPartition(),
                vmi.getServiceOrder().getBusinessName(),
                chip,
                chip.getRegionalOffice()
        );
    }

}
