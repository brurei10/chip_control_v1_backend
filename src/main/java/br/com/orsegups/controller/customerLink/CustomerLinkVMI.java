package br.com.orsegups.controller.customerLink;

import br.com.orsegups.dto.*;

public class CustomerLinkVMI {

    private ChipDTO chip;
    private TechnicianDTO technician;
    private ServiceOrderDTO serviceOrder;
    private Boolean confirmReplacement;
    private String searchText;
    private TableOptionDTO tableOption;
    private String reason;
    private Long customerId;

    public ChipDTO getChip() {
        return chip;
    }

    public void setChip(ChipDTO chip) {
        this.chip = chip;
    }

    public TechnicianDTO getTechnician() {
        return technician;
    }

    public void setTechnician(TechnicianDTO technician) {
        this.technician = technician;
    }

    public ServiceOrderDTO getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(ServiceOrderDTO serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    public Boolean getConfirmReplacement() {
        return confirmReplacement;
    }

    public void setConfirmReplacement(Boolean confirmReplacement) {
        this.confirmReplacement = confirmReplacement;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
}