package br.com.orsegups.controller.customerLink;

import br.com.orsegups.dto.CustomerLinkDTO;
import br.com.orsegups.dto.TableOptionDTO;
import br.com.orsegups.entity.CustomerLink;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerLinkVMO {

    private List<CustomerLinkDTO> customerLinks;
    private TableOptionDTO tableOption;
    private List<CustomerLinkDTO> customerLinksToReplace;

    public CustomerLinkVMO() {
        super();
    }

    public CustomerLinkVMO(List<CustomerLink> customerLinksToReplace) {
        this.setCustomerLinksToReplace(customerLinksToReplace);
    }

    public List<CustomerLinkDTO> getCustomerLinks() {
        return customerLinks;
    }

    public void setCustomerLinks(List<CustomerLink> customerLinks) {
        this.customerLinks = customerLinks.stream().map(CustomerLinkDTO::new).collect(Collectors.toList());
    }

    public void setCustomerLinkDTOs(List<CustomerLinkDTO> customerLinks) {
        this.customerLinks = customerLinks;
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }

    public List<CustomerLinkDTO> getCustomerLinksToReplace() {
        return customerLinksToReplace;
    }

    public void setCustomerLinksToReplace(List<CustomerLink> customerLinksToReplace) {
        this.customerLinksToReplace = customerLinksToReplace.stream().map(CustomerLinkDTO::new).collect(Collectors.toList());
    }
}
