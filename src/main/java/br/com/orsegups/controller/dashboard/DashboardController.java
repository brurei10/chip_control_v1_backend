package br.com.orsegups.controller.dashboard;

import br.com.orsegups.configuration.annotation.ChipControlController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import javax.inject.Inject;

@ChipControlController("/api/dashboard")
public class DashboardController {

    @Inject
    private DashboardFacade dashboardFacade;

    @GetMapping(value = "/stats")
    public ResponseEntity<DashboardVMO> getStats() {
        return new ResponseEntity<>(dashboardFacade.getStats(), HttpStatus.OK);
    }

    @GetMapping(value = "/stockStats")
    public ResponseEntity<DashboardVMO> getStockStats() {
        return new ResponseEntity<>(dashboardFacade.getStockStats(), HttpStatus.OK);
    }

    @GetMapping(value = "/operatorStats")
    public ResponseEntity<DashboardVMO> getOperatorStats() {
        return new ResponseEntity<>(dashboardFacade.getOperatorStats(), HttpStatus.OK);
    }

    @GetMapping(value = "/officeStats")
    public ResponseEntity<DashboardVMO> getOfficeStats() {
        return new ResponseEntity<>(dashboardFacade.getOfficeStats(), HttpStatus.OK);
    }

    @GetMapping(value = "/equipmentStats")
    public ResponseEntity<DashboardVMO> getEquipmentStats() {
        return new ResponseEntity<>(dashboardFacade.getEquipmentStats(), HttpStatus.OK);
    }

}
