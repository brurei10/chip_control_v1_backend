package br.com.orsegups.controller.equipmentLink;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.EquipmentLinkDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.inject.Inject;

@ChipControlController("/api/equipmentLink")
public class EquipmentLinkController {

    @Inject
    private EquipmentLinkFacade equipmentLinkFacade;

    @PostMapping(value = "/create")
    public ResponseEntity<EquipmentLinkDTO> create(@RequestBody EquipmentLinkVMI vmi) {
        return new ResponseEntity<>(equipmentLinkFacade.create(vmi), HttpStatus.CREATED);
    }

    @PostMapping(value = "/listLinked")
    public ResponseEntity<EquipmentLinkVMO> listLinked(@RequestBody EquipmentLinkVMI vmi) {
        return new ResponseEntity<>(equipmentLinkFacade.listLinked(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/cancel")
    public ResponseEntity<EquipmentLinkDTO> cancel(@RequestBody EquipmentLinkVMI vmi) {
        return new ResponseEntity<>(equipmentLinkFacade.cancel(vmi), HttpStatus.OK);
    }

}
