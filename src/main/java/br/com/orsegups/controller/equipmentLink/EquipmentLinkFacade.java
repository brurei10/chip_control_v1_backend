package br.com.orsegups.controller.equipmentLink;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.EquipmentLinkDTO;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.dto.TableOptionDTO;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.EquipmentLink;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.service.ChipHistoryService;
import br.com.orsegups.service.ChipService;
import br.com.orsegups.service.EquipmentLinkService;
import br.com.orsegups.service.RegionalOfficeService;
import br.com.orsegups.service.utils.CancelUtilService;
import br.com.orsegups.util.SessionUtil;
import br.com.orsegups.validator.ChipValidator;
import br.com.orsegups.validator.EquipmentLinkValidator;
import br.com.orsegups.validator.RegionalOfficeValidator;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.inject.Inject;

@ChipControlFacade
public class EquipmentLinkFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private EquipmentLinkService equipmentLinkService;
    @Inject
    private RegionalOfficeService regionalOfficeService;
    @Inject
    private CancelUtilService cancelUtilService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private ChipValidator chipValidator;
    @Inject
    private EquipmentLinkValidator equipmentLinkValidator;
    @Inject
    private RegionalOfficeValidator regionalOfficeValidator;
    @Inject
    private SessionUtil sessionUtil;

    public EquipmentLinkDTO create(EquipmentLinkVMI vmi) {
        RegionalOffice regional = regionalOfficeService.findById(vmi.getRegional().getId());
        regionalOfficeValidator.validateUserInRegional(regional);
        equipmentLinkValidator.validateRegionalIsNotOffice(regional);

        Chip chip = chipService.findById(vmi.getChip().getId());
        chipValidator.validateChipInRegionalStockOrTechnician(chip, regional);

        EquipmentLink equipmentLink = getEntityFromVMI(vmi, chip);
        chip.setEquipmentLink(equipmentLink);
        chipService.save(chip);
        equipmentLink = equipmentLinkService.save(equipmentLink);

        chipHistoryService.equipmentLink(equipmentLink);
        return new EquipmentLinkDTO(equipmentLink);
    }

    public EquipmentLinkVMO listLinked(EquipmentLinkVMI vmi) {
        EquipmentLinkVMO vmo = new EquipmentLinkVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("el.installation_date"), Sort.Order.desc("el.id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage() - 1, tableOption.getItemPerPage(), sort);
        PageDTO<EquipmentLink> page = equipmentLinkService.findLinkedByFilters(vmi.getSearchText(), sessionUtil.getCurrentUserRegionalIds(), pg);

        vmo.setEquipmentLinks(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;
    }


    public EquipmentLinkDTO cancel(EquipmentLinkVMI vmi) {
        EquipmentLink equipmentLink = equipmentLinkService.findById(vmi.getLinkId());
        cancelUtilService.requestChipCancellation(equipmentLink.getChip(), vmi.getReason());
        return new EquipmentLinkDTO();
    }

    private EquipmentLink getEntityFromVMI(EquipmentLinkVMI vmi, Chip chip) {
        return new EquipmentLink(
                chip,
                vmi.getIdentifier(),
                chip.getRegionalOffice()
        );
    }

}
