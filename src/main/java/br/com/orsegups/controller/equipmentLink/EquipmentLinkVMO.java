package br.com.orsegups.controller.equipmentLink;

import br.com.orsegups.dto.EquipmentLinkDTO;
import br.com.orsegups.dto.TableOptionDTO;
import br.com.orsegups.entity.EquipmentLink;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EquipmentLinkVMO {

    private List<EquipmentLinkDTO> equipmentLinks;
    private TableOptionDTO tableOption;

    public EquipmentLinkVMO() {
        super();
    }

    public List<EquipmentLinkDTO> getEquipmentLinks() {
        return equipmentLinks;
    }

    public void setEquipmentLinks(List<EquipmentLink> equipmentLinks) {
        this.equipmentLinks = equipmentLinks.stream().map(EquipmentLinkDTO::new).collect(Collectors.toList());
    }

    public void setEquipmentLinkDTOs(List<EquipmentLinkDTO> equipmentLinks) {
        this.equipmentLinks = equipmentLinks;
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }

}
