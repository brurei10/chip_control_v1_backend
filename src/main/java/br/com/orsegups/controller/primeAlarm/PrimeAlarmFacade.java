package br.com.orsegups.controller.primeAlarm;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.configuration.locale.Messages;
import br.com.orsegups.entity.*;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.service.*;
import br.com.orsegups.service.utils.CancelUtilService;

import javax.inject.Inject;
import java.util.List;

@ChipControlFacade
public class PrimeAlarmFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private OperatorService operatorService;
    @Inject
    private RegionalOfficeService regionalOfficeService;
    @Inject
    private CompanyService companyService;
    @Inject
    private CustomerLinkService customerLinkService;
    @Inject
    private CancelUtilService cancelUtilService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private Messages messages;

    public Boolean linkCustomerChip(PrimeAlarmVMI vmi) {
        Chip chip = getOrCreateChip(vmi);

        List<CustomerLink> activeLinks = customerLinkService.findByRemovalDateIsNullAndCompanyIdAndCentralId(
                vmi.getCompanyId(), vmi.getCentralId());

        if (checkIfChipAlreadyLinked(activeLinks, chip)) return true;

        if (activeLinks.size() > 0)
            cancelUtilService.requestChipCancellationByCustomer(activeLinks, messages.get("primeAlarm.removeLink.reason"));

        CustomerLink customerLink = createNewLink(vmi, chip);
        chip.setCustomerLink(customerLink);
        chipService.save(chip);

        return true;
    }

    private boolean checkIfChipAlreadyLinked(List<CustomerLink> activeLinks, Chip chip) {
        return activeLinks.stream().anyMatch(customerLink -> customerLink.getChip().equals(chip));
    }

    private CustomerLink createNewLink(PrimeAlarmVMI vmi, Chip chip) {
        CustomerLink customerLink = new CustomerLink(
                vmi.getCompanyId(),
                vmi.getCentralId(),
                vmi.getCentralPartition(),
                vmi.getBusinessName(),
                chip,
                chip.getRegionalOffice()
        );


        customerLink = customerLinkService.save(customerLink);
        chipHistoryService.customerLink(customerLink);
        return customerLink;
    }

    private Chip getOrCreateChip(PrimeAlarmVMI vmi) {
        return chipService.findOptionalByIccId(vmi.getIccId()).orElseGet(() -> createChip(vmi));
    }

    private Chip createChip(PrimeAlarmVMI vmi) {
        Operator operator = operatorService.findByName(vmi.getOperatorName());
        if (operator == null)
            throw new CustomException(messages.get("entity.operator.notFound", vmi.getOperatorName()));
        RegionalOffice regionalOffice = regionalOfficeService.findOrsegupsPrime();
        Company company = companyService.findOrsegups();
        Chip chip = chipService.save(new Chip(vmi.getIccId(), company, operator, regionalOffice));
        chipHistoryService.chipCreated(chip);
        return chip;
    }

}
