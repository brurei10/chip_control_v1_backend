package br.com.orsegups.controller.report;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.service.ChipService;
import br.com.orsegups.service.RegionalOfficeService;
import br.com.orsegups.util.report.ReportTransformer;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.PageRequest;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class ReportFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private RegionalOfficeService regionalOfficeService;
    @Inject
    private ReportTransformer reportTransformer;

    public InputStreamResource reportInCancellation(ReportVMI vmi) {
        try {
            List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
            PageDTO<Chip> page = chipService.findToCancelByFilters(vmi.getSearchText(), regionalIds, PageRequest.of(0, Integer.MAX_VALUE));

            List<ChipDTO> chips = page.getItems().stream().map(ChipDTO::new).collect(Collectors.toList());
            return new InputStreamResource(reportTransformer.chipInCancellationToXls(chips));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
