package br.com.orsegups.controller.report;

import br.com.orsegups.dto.RegionalOfficeDTO;

public class ReportVMI {

    private String searchText;
    private RegionalOfficeDTO regional;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public RegionalOfficeDTO getRegional() {
        return regional;
    }

    public void setRegional(RegionalOfficeDTO regional) {
        this.regional = regional;
    }
}
