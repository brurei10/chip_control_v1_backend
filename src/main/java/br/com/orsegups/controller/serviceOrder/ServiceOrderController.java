package br.com.orsegups.controller.serviceOrder;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.ServiceOrderDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import javax.inject.Inject;
import java.util.List;

@ChipControlController("/api/serviceOrder")
public class ServiceOrderController {

    @Inject
    private ServiceOrderFacade serviceOrderFacade;

    @GetMapping(value = "/list")
    public ResponseEntity<List<ServiceOrderDTO>> list() {
        return new ResponseEntity<>(serviceOrderFacade.list(), HttpStatus.OK);
    }
}
