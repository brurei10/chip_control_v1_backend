package br.com.orsegups.controller.technician;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.TechnicianDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.inject.Inject;
import java.util.List;

@ChipControlController("/api/technician")
public class TechnicianController {

    @Inject
    private TechnicianFacade technicianFacade;

    @GetMapping(value = "/list")
    public ResponseEntity<List<TechnicianDTO>> list() {
        return new ResponseEntity<>(technicianFacade.list(), HttpStatus.OK);
    }

    @GetMapping(value = "/chipAmount/{technicianId}")
    public ResponseEntity<Integer> countChipWithTechnician(@PathVariable("technicianId") Long technicianId) {
        return new ResponseEntity<>(technicianFacade.countChipWithTechnician(technicianId), HttpStatus.OK);
    }
}
