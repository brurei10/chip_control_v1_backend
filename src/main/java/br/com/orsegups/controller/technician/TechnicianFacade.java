package br.com.orsegups.controller.technician;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.TechnicianDTO;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.externalService.SigmaService;
import br.com.orsegups.service.ChipService;
import br.com.orsegups.util.SessionUtil;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class TechnicianFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private SigmaService sigmaService;
    @Inject
    private SessionUtil sessionUtil;

    public List<TechnicianDTO> list() {
        List<String> regionalAcronyms = sessionUtil.getCurrentUserRegional().stream()
                .map(RegionalOffice::getAcronym).collect(Collectors.toList());
        List<TechnicianDTO> technicians = sigmaService.findTechniciansByRegionals(regionalAcronyms);
        if (technicians != null)
            technicians.sort(Comparator.comparing(TechnicianDTO::getName));
        return technicians;

    }

    public Integer countChipWithTechnician(Long technicianId) {
        return chipService.countByTechnicianId(technicianId);
    }

}
