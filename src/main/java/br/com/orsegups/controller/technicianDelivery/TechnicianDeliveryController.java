package br.com.orsegups.controller.technicianDelivery;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.TechnicianDeliveryDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.inject.Inject;

@ChipControlController("/api/technicianDelivery")
public class TechnicianDeliveryController {

    @Inject
    private TechnicianDeliveryFacade technicianDeliveryFacade;

    @PostMapping(value = "/create")
    public ResponseEntity<TechnicianDeliveryDTO> create(@RequestBody TechnicianDeliveryVMI vmi) {
        return new ResponseEntity<>(technicianDeliveryFacade.create(vmi), HttpStatus.CREATED);
    }
}
