package br.com.orsegups.controller.technicianDelivery;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.TechnicianDeliveryDTO;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.entity.TechnicianDelivery;
import br.com.orsegups.entity.TechnicianDeliveryChip;
import br.com.orsegups.service.ChipHistoryService;
import br.com.orsegups.service.ChipService;
import br.com.orsegups.service.TechnicianDeliveryService;
import br.com.orsegups.util.SessionUtil;
import br.com.orsegups.validator.ChipValidator;
import br.com.orsegups.validator.TechnicianDeliveryValidator;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class TechnicianDeliveryFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private TechnicianDeliveryService technicianDeliveryService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private TechnicianDeliveryValidator technicianDeliveryValidator;
    @Inject
    private ChipValidator chipValidator;
    @Inject
    private SessionUtil sessionUtil;

    public TechnicianDeliveryDTO create(TechnicianDeliveryVMI vmi) {
        technicianDeliveryValidator.validateCreate(vmi);
        TechnicianDelivery technicianDelivery = getEntityFromVMI(vmi);

        List<Chip> chips = technicianDelivery.getTechnicianDeliveryChips().stream()
                .map(TechnicianDeliveryChip::getChip).collect(Collectors.toList());
        chipService.updateTechnician(chips, technicianDelivery);

        technicianDelivery = technicianDeliveryService.save(technicianDelivery);
        chipHistoryService.technicianDelivered(chips, technicianDelivery);
        return new TechnicianDeliveryDTO(technicianDelivery);
    }

    private TechnicianDelivery getEntityFromVMI(TechnicianDeliveryVMI vmi) {
        TechnicianDelivery technicianDelivery = new TechnicianDelivery();

        List<RegionalOffice> regionalOffices = sessionUtil.getCurrentUserRegional();
        List<Long> chipIds = vmi.getChips().stream().map(ChipDTO::getId).collect(Collectors.toList());
        List<Chip> chips = chipService.findAllById(chipIds);
        chipValidator.validateAllChipsInStock(chips, regionalOffices);
        chips.forEach(chip -> new TechnicianDeliveryChip(technicianDelivery, chip));

        technicianDelivery.setTechnicianId(vmi.getTechnician().getId());
        technicianDelivery.setTechnicianName(vmi.getTechnician().getName());
        String technicianRegionalAcronym = vmi.getTechnician().getName().split("-")[0].trim();
        RegionalOffice regionalOffice = regionalOffices.stream()
                .filter(regional -> regional.getAcronym().equals(technicianRegionalAcronym)).findFirst().get();
        technicianDelivery.setRegionalOffice(regionalOffice);

        return technicianDelivery;
    }
}
