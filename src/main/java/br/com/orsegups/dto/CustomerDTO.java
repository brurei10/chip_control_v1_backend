package br.com.orsegups.dto;

public class CustomerDTO {

    private String centralId;
    private Long companyId;

    public CustomerDTO() {
        super();
    }

    public CustomerDTO(String centralId, Long companyId) {
        this();
        this.centralId = centralId;
        this.companyId = companyId;
    }

    public String getCentralId() {
        return centralId;
    }

    public void setCentralId(String centralId) {
        this.centralId = centralId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
