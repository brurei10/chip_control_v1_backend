package br.com.orsegups.dto;

public class RegionalStatsDTO {

    private RegionalOfficeDTO office;
    private Integer officeAmount;
    private Integer stockAmount;
    private Integer technicianAmount;
    private Integer customerAmount;
    private Integer inTransitAmount;

    public RegionalStatsDTO() {
        super();
    }

    public RegionalStatsDTO(RegionalOfficeDTO office) {
        this();
        this.office = office;
    }

    public RegionalOfficeDTO getOffice() {
        return office;
    }

    public void setOffice(RegionalOfficeDTO office) {
        this.office = office;
    }

    public Integer getOfficeAmount() {
        return officeAmount;
    }

    public void setOfficeAmount(Integer officeAmount) {
        this.officeAmount = officeAmount;
    }

    public Integer getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(Integer stockAmount) {
        this.stockAmount = stockAmount;
    }

    public Integer getTechnicianAmount() {
        return technicianAmount;
    }

    public void setTechnicianAmount(Integer technicianAmount) {
        this.technicianAmount = technicianAmount;
    }

    public Integer getCustomerAmount() {
        return customerAmount;
    }

    public void setCustomerAmount(Integer customerAmount) {
        this.customerAmount = customerAmount;
    }

    public Integer getInTransitAmount() {
        return inTransitAmount;
    }

    public void setInTransitAmount(Integer inTransitAmount) {
        this.inTransitAmount = inTransitAmount;
    }
}
