package br.com.orsegups.dto;

public class TechnicianDTO {

    private Long id;
    private String username;
    private String name;

    public TechnicianDTO() {
        super();
    }

    public TechnicianDTO(Long id, String username, String name) {
        this();
        this.id = id;
        this.username = username;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}