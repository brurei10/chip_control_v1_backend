package br.com.orsegups.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class Chip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20, nullable = false, unique = true)
    private String iccId;

    @Column(length = 50)
    private String phoneNumber;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(nullable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date requestedCancellationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date cancellationDate;

    @Column(length = 500)
    private String cancellationReason;

    @ManyToOne
    private RegionalOffice canceledFromRegional;

    @Column(nullable = false)
    private Boolean toRemove;

    @ManyToOne(fetch = FetchType.EAGER)
    private Company company;

    @ManyToOne(fetch = FetchType.EAGER)
    private Operator operator;

    @ManyToOne(fetch = FetchType.EAGER)
    private RegionalOffice regionalOffice;

    @ManyToOne
    private TechnicianDelivery technicianDelivery;

    @ManyToOne
    private CustomerLink customerLink;

    @ManyToOne
    private EquipmentLink equipmentLink;

    public Chip() {
        super();
        this.createdDate = new Date();
        this.toRemove = false;

    }

    public Chip(String iccId, Company company, Operator operator, RegionalOffice regionalOffice) {
        this();
        this.iccId = iccId;
        this.company = company;
        this.operator = operator;
        this.regionalOffice = regionalOffice;
    }

    @Override
    public String toString() {
        return "Chip{" +
                "id=" + id +
                ", iccId='" + iccId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", createdDate=" + createdDate +
                ", requestedCancellationDate=" + requestedCancellationDate +
                ", cancellationDate=" + cancellationDate +
                ", cancellationReason='" + cancellationReason + '\'' +
                ", canceledFromRegional='" + canceledFromRegional + '\'' +
                ", toRemove=" + toRemove +
                ", company=" + company +
                ", operator=" + operator +
                ", regionalOffice=" + regionalOffice +
                ", technicianDelivery=" + technicianDelivery +
                ", customerLink=" + customerLink +
                ", equipmentLink=" + equipmentLink +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chip chip = (Chip) o;
        return id.equals(chip.id) && iccId.equals(chip.iccId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, iccId);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getRequestedCancellationDate() {
        return requestedCancellationDate;
    }

    public void setRequestedCancellationDate(Date requestedCancellationDate) {
        this.requestedCancellationDate = requestedCancellationDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public RegionalOffice getCanceledFromRegional() {
        return canceledFromRegional;
    }

    public void setCanceledFromRegional(RegionalOffice canceledFromRegional) {
        this.canceledFromRegional = canceledFromRegional;
    }

    public Boolean getToRemove() {
        return toRemove;
    }

    public void setToRemove(Boolean toRemove) {
        this.toRemove = toRemove;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public RegionalOffice getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(RegionalOffice regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public TechnicianDelivery getTechnicianDelivery() {
        return technicianDelivery;
    }

    public void setTechnicianDelivery(TechnicianDelivery technicianDelivery) {
        this.technicianDelivery = technicianDelivery;
    }

    public CustomerLink getCustomerLink() {
        return customerLink;
    }

    public void setCustomerLink(CustomerLink customerLink) {
        this.technicianDelivery = null;
        this.customerLink = customerLink;
    }

    public EquipmentLink getEquipmentLink() {
        return equipmentLink;
    }

    public void setEquipmentLink(EquipmentLink equipmentLink) {
        this.technicianDelivery = null;
        this.equipmentLink = equipmentLink;
    }
}
