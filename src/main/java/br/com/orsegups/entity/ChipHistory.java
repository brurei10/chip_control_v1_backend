package br.com.orsegups.entity;

import br.com.orsegups.entity.type.HistoryType;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class ChipHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20, nullable = false)
    private String iccId;

    @ManyToOne
    private Chip chip;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    @Column(nullable = false)
    private Date historyDate;

    @Type(type = "br.com.orsegups.entity.type.HistoryUserType")
    @Column(nullable = false)
    private HistoryType historyType;

    @Column(length = 500)
    private String observation;

    private String user;

    @Column(length = 20)
    private String userType;

    @ManyToOne(fetch = FetchType.EAGER)
    private RegionalOffice regionalOffice;

    @ManyToOne
    private ChipLot chipLot;

    @ManyToOne
    private TechnicianDelivery technicianDelivery;

    @ManyToOne
    private CustomerLink customerLink;

    @ManyToOne
    private EquipmentLink equipmentLink;

    public ChipHistory() {
        super();
    }

    public ChipHistory(Chip chip, Date historyDate, HistoryType historyType) {
        this();
        this.chip = chip;
        this.iccId = chip.getIccId();
        this.regionalOffice = chip.getRegionalOffice();
        this.historyDate = historyDate;
        this.historyType = historyType;
    }

    public ChipHistory(Chip chip, Date historyDate, ChipLot chipLot, HistoryType historyType) {
        this(chip, historyDate, historyType);
        this.chipLot = chipLot;
    }

    public ChipHistory(Chip chip, Date historyDate, TechnicianDelivery technicianDelivery, HistoryType historyType) {
        this(chip, historyDate, historyType);
        this.technicianDelivery = technicianDelivery;
    }

    public ChipHistory(Chip chip, Date historyDate, CustomerLink customerLink, HistoryType historyType) {
        this(chip, historyDate, historyType);
        this.customerLink = customerLink;
    }

    public ChipHistory(Chip chip, Date historyDate, EquipmentLink equipmentLink, HistoryType historyType) {
        this(chip, historyDate, historyType);
        this.equipmentLink = equipmentLink;
    }

    public ChipHistory(Chip chip, Date historyDate, String observation, HistoryType historyType) {
        this(chip, historyDate, historyType);
        this.observation = observation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChipHistory that = (ChipHistory) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public Chip getChip() {
        return chip;
    }

    public void setChip(Chip chip) {
        this.chip = chip;
    }

    public Date getHistoryDate() {
        return historyDate;
    }

    public void setHistoryDate(Date historyDate) {
        this.historyDate = historyDate;
    }

    public HistoryType getHistoryType() {
        return historyType;
    }

    public void setHistoryType(HistoryType historyType) {
        this.historyType = historyType;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public RegionalOffice getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(RegionalOffice regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public ChipLot getChipLot() {
        return chipLot;
    }

    public void setChipLot(ChipLot chipLot) {
        this.chipLot = chipLot;
    }

    public TechnicianDelivery getTechnicianDelivery() {
        return technicianDelivery;
    }

    public void setTechnicianDelivery(TechnicianDelivery technicianDelivery) {
        this.technicianDelivery = technicianDelivery;
    }

    public CustomerLink getCustomerLink() {
        return customerLink;
    }

    public void setCustomerLink(CustomerLink customerLink) {
        this.customerLink = customerLink;
    }

    public EquipmentLink getEquipmentLink() {
        return equipmentLink;
    }

    public void setEquipmentLink(EquipmentLink equipmentLink) {
        this.equipmentLink = equipmentLink;
    }
}
