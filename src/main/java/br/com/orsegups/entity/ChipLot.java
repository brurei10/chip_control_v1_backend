package br.com.orsegups.entity;

import br.com.orsegups.entity.type.ChipLotStatusType;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class ChipLot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date sendDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date expectedArrivalDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date arrivalDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date cancellationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private RegionalOffice regionalOrigin;

    @ManyToOne(fetch = FetchType.EAGER)
    private RegionalOffice regionalDestination;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "chip_lot_id")
    private List<ChipLotItem> chipLotItems;

    private Integer sentAmount;

    private Integer receivedAmount;

    @Type(type = "br.com.orsegups.entity.type.ChipLotStatusUserType")
    @Column(nullable = false)
    private ChipLotStatusType status;

    public ChipLot() {
        super();
        this.createdDate = new Date();
        this.sentAmount = 0;
        this.receivedAmount = 0;
    }

    public ChipLot(Date createdDate, Date sendDate, Date expectedArrivalDate, Date arrivalDate, Date cancellationDate, RegionalOffice regionalOrigin,
                   RegionalOffice regionalDestination, List<ChipLotItem> chipLotItems, Integer sentAmount, Integer receivedAmount, ChipLotStatusType status) {
        this.createdDate = createdDate;
        this.sendDate = sendDate;
        this.expectedArrivalDate = expectedArrivalDate;
        this.arrivalDate = arrivalDate;
        this.cancellationDate = cancellationDate;
        this.regionalOrigin = regionalOrigin;
        this.regionalDestination = regionalDestination;
        this.chipLotItems = chipLotItems;
        this.sentAmount = sentAmount;
        this.receivedAmount = receivedAmount;
        this.status = status;
    }

    @Override
    public String toString() {
        return "ChipLot{" +
                "id=" + id +
                ", createdDate=" + createdDate +
                ", sendDate=" + sendDate +
                ", expectedArrivalDate=" + expectedArrivalDate +
                ", arrivalDate=" + arrivalDate +
                ", cancellationDate=" + cancellationDate +
                ", regionalOrigin=" + regionalOrigin +
                ", regionalDestination=" + regionalDestination +
                ", chipLotItems=" + chipLotItems +
                ", sentAmount=" + sentAmount +
                ", receivedAmount=" + receivedAmount +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChipLot chipLot = (ChipLot) o;
        return id.equals(chipLot.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(Date expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public RegionalOffice getRegionalOrigin() {
        return regionalOrigin;
    }

    public void setRegionalOrigin(RegionalOffice regionalOrigin) {
        this.regionalOrigin = regionalOrigin;
    }

    public RegionalOffice getRegionalDestination() {
        return regionalDestination;
    }

    public void setRegionalDestination(RegionalOffice regionalDestination) {
        this.regionalDestination = regionalDestination;
    }

    public List<ChipLotItem> getChipLotItems() {
        return chipLotItems;
    }

    public void setChipLotItems(List<ChipLotItem> chipLotItems) {
        if (chipLotItems != null && !chipLotItems.isEmpty()) this.sentAmount = chipLotItems.size();
        this.chipLotItems = chipLotItems;
    }

    public void addChipLotItem(ChipLotItem chipLotItem) {
        if (this.chipLotItems == null) this.chipLotItems = new ArrayList<>();
        this.chipLotItems.add(chipLotItem);
    }

    public Integer getSentAmount() {
        return sentAmount;
    }

    public void setSentAmount(Integer sentAmount) {
        this.sentAmount = sentAmount;
    }

    public Integer getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Integer receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public ChipLotStatusType getStatus() {
        return status;
    }

    public void setStatus(ChipLotStatusType status) {
        this.status = status;
    }
}
