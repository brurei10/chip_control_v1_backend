package br.com.orsegups.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class TechnicianDelivery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sigma_technician_id", nullable = false)
    private Long technicianId;

    @Column(name = "sigma_technician_name", nullable = false, length = 200)
    private String technicianName;

    @ManyToOne(fetch = FetchType.EAGER)
    private RegionalOffice regionalOffice;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date deliveryDate;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "technician_delivery_id")
    private List<TechnicianDeliveryChip> technicianDeliveryChips;

    public TechnicianDelivery() {
        super();
        this.deliveryDate = new Date();
    }

    public TechnicianDelivery(Long technicianId, String technicianName, RegionalOffice regionalOffice, List<TechnicianDeliveryChip> technicianDeliveryChips) {
        this();
        this.technicianId = technicianId;
        this.technicianName = technicianName;
        this.regionalOffice = regionalOffice;
        this.technicianDeliveryChips = technicianDeliveryChips;
    }

    @Override
    public String toString() {
        return "TechnicianDelivery{" +
                "id=" + id +
                ", technicianId=" + technicianId +
                ", technicianName='" + technicianName + '\'' +
                ", regionalOffice=" + regionalOffice +
                ", deliveryDate=" + deliveryDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechnicianDelivery that = (TechnicianDelivery) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(Long technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public RegionalOffice getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(RegionalOffice regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public List<TechnicianDeliveryChip> getTechnicianDeliveryChips() {
        return technicianDeliveryChips;
    }

    public void setTechnicianDeliveryChips(List<TechnicianDeliveryChip> technicianDeliveryChips) {
        this.technicianDeliveryChips = technicianDeliveryChips;
    }

    public void addTechnicianDeliveryChip(TechnicianDeliveryChip technicianDeliveryChip) {
        if (this.technicianDeliveryChips == null) this.technicianDeliveryChips = new ArrayList<>();
        this.technicianDeliveryChips.add(technicianDeliveryChip);
    }
}
