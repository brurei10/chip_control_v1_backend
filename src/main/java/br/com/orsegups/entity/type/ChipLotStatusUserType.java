package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnumUserType;

public class ChipLotStatusUserType extends PersistentEnumUserType<ChipLotStatusType> {

    @Override
    public Class<ChipLotStatusType> returnedClass() {
        return ChipLotStatusType.class;
    }

}