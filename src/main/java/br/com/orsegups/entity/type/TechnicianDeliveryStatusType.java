package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnum;

public enum TechnicianDeliveryStatusType implements PersistentEnum {
    DELIVERED(1L),
    RETURNED(2L);

    private final Long code;

    private TechnicianDeliveryStatusType(Long code) {
        this.code = code;
    }

    public static TechnicianDeliveryStatusType valueOf(Long code) {
        for (TechnicianDeliveryStatusType technicianDeliveryStatusType : values()) {
            if (technicianDeliveryStatusType.code.equals(code))
                return technicianDeliveryStatusType;
        }
        return null;
    }

    public Long getCode() {
        return code;
    }

}