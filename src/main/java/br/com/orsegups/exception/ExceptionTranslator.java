package br.com.orsegups.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 */
@ControllerAdvice
public class ExceptionTranslator {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ExceptionDTO> processCustomException(CustomException e) {
        log.error("CustomException - Error detail: " + e.toString());
        return generateResponse(CustomExceptionCode.CUSTOM, e.getClass(), e.getMessage());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ExceptionDTO> processEntityNotFoundExceptionException(EntityNotFoundException e) {
        log.error("EntityNotFoundException - Error detail: " + e.toString());
        return generateResponse(CustomExceptionCode.ENTITY_NOT_FOUND, e.getClass(), e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionDTO> processGenericException(Exception e) {
        log.error("Exception", e);
        return generateResponse(CustomExceptionCode.GENERIC, e.getClass(), e.getMessage());

    }

    private ResponseEntity<ExceptionDTO> generateResponse(CustomExceptionCode code, Class clazz, String errorMessage) {
        ExceptionDTO exceptionDTO = new ExceptionDTO(clazz.getSimpleName(), errorMessage);
        return ResponseEntity.status(code.getCode()).body(exceptionDTO);
    }
}


