package br.com.orsegups.externalService;

import br.com.orsegups.configuration.locale.Messages;
import br.com.orsegups.dto.CustomerToCancelDTO;
import br.com.orsegups.dto.ServiceOrderDTO;
import br.com.orsegups.dto.TechnicianDTO;
import br.com.orsegups.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class SigmaService {

    private static final Logger log = LoggerFactory.getLogger(SigmaService.class);

    @Inject
    private RestTemplate restTemplate;
    @Inject
    protected Messages messages;

    @Value("${chipControl.service.sigmaService.url}")
    private String sigmaServiceBaseURL;

    @Value("${chipControl.service.sigmaService.token}")
    private String sigmaServiceToken;

    private static final String LIST_TECHNICIANS = "/chip/listTechnicians?regionalAcronyms=";
    private static final String GET_SERVICE_ORDER_BY_USER = "/chip/getServiceOrderByUser?username=";
    private static final String GET_SERVICE_ORDER_BY_REGIONAL = "/chip/getServiceOrderByRegional?regionalAcronyms=";
    private static final String GET_CUSTOMER_TO_CANCEL = "/chip/getCustomerToCancel?cutoffDate=";


    public List<TechnicianDTO> findTechniciansByRegionals(List<String> regionalAcronyms) {
        try {
            String regionals = regionalAcronyms.stream().collect(Collectors.joining(","));
            return restTemplate.exchange(sigmaServiceBaseURL + LIST_TECHNICIANS + regionals, HttpMethod.GET, getRequestEntity(),
                    new ParameterizedTypeReference<SigmaResponseWrapper<List<TechnicianDTO>>>() {}).getBody().getRet();
        } catch (Exception e) {
            log.error("Erro ao buscar técnicos. " + e.getMessage());
            throw new CustomException(messages.get("sigmaService.technician.error"));
        }
    }

    public List<ServiceOrderDTO> findServiceOrderByUser(String username) {
        return findServiceOrders(sigmaServiceBaseURL + GET_SERVICE_ORDER_BY_USER + username);
    }

    public List<ServiceOrderDTO> findServiceOrderByRegional(List<String> regionalAcronyms) {
        String regionals = regionalAcronyms.stream().collect(Collectors.joining(","));
        return findServiceOrders(sigmaServiceBaseURL + GET_SERVICE_ORDER_BY_REGIONAL + regionals);
    }

    private List<ServiceOrderDTO> findServiceOrders(String url) {
        try {
            return restTemplate.exchange(url, HttpMethod.GET, getRequestEntity(),
                    new ParameterizedTypeReference<SigmaResponseWrapper<List<ServiceOrderDTO>>>() {}).getBody().getRet();
        } catch (Exception e) {
            log.error("Erro ao buscar orderns de serviço. " + e.getMessage());
            throw new CustomException(messages.get("sigmaService.serviceOrder.error"));
        }
    }
    public CustomerToCancelDTO getCustomerToCancel(String cutoffDate) {
        try {
            return restTemplate.exchange(sigmaServiceBaseURL + GET_CUSTOMER_TO_CANCEL + cutoffDate, HttpMethod.GET, getRequestEntity(),
                    new ParameterizedTypeReference<SigmaResponseWrapper<CustomerToCancelDTO>>() {}).getBody().getRet();
        } catch (Exception e) {
            log.error("Erro ao buscar clientes a serem cancelados. " + e.getMessage());
            throw new CustomException(messages.get("sigmaService.customerToCancel.error"));
        }
    }

    private HttpEntity<String> getRequestEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-AUTH-TOKEN", sigmaServiceToken);

        return new HttpEntity<>(headers);
    }

}
