package br.com.orsegups.repository;

import br.com.orsegups.entity.ChipHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChipHistoryRepository extends JpaRepository<ChipHistory, Long> {

    List<ChipHistory> findByChipId(Long chipId);

}
