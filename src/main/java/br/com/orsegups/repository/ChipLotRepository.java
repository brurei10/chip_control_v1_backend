package br.com.orsegups.repository;

import br.com.orsegups.entity.ChipLot;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.entity.type.ChipLotStatusType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChipLotRepository extends JpaRepository<ChipLot, Long>, ChipLotRepositoryInterface {

    List<ChipLot> findAllByOrderBySendDateDesc();

    List<ChipLot> findAllByRegionalDestinationOrderBySendDateDesc(RegionalOffice regional);

    List<ChipLot> findAllByRegionalOriginOrderBySendDateDesc(RegionalOffice regional);

    Integer countByStatusAndRegionalDestinationId(ChipLotStatusType status, Long regionalId);
}
