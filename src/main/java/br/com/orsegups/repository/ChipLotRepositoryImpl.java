package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.ChipLot;
import br.com.orsegups.entity.type.ChipLotStatusType;
import br.com.orsegups.repository.utils.AbstractRepositoryImpl;
import br.com.orsegups.repository.utils.QueryObject;
import br.com.orsegups.util.DateUtil;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Component
public class ChipLotRepositoryImpl extends AbstractRepositoryImpl<ChipLot> implements ChipLotRepositoryInterface {

    public PageDTO<ChipLot> findListToReceiveByFilters(String searchText, List<Long> regionalDestinationIds, Boolean lateOnly, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, null, regionalDestinationIds, lateOnly);
        return executePagedQuery(queryObject, pageable, "cl");
    }

    public PageDTO<ChipLot> findSentListByFilters(String searchText, List<Long> regionalOriginIds, List<Long> regionalDestinationIds, Boolean lateOnly, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, regionalOriginIds, regionalDestinationIds, lateOnly);
        return executePagedQuery(queryObject, pageable, "cl");
    }

    private QueryObject getQueryObject(String searchText, List<Long> regionalOriginIds, List<Long> regionalDestinationIds, Boolean lateOnly) {
        QueryObject queryObject = new QueryObject();
        StringBuilder queryString = new StringBuilder(" FROM chip_lot cl INNER JOIN chip_lot_item cli ON cli.chip_lot_id = cl.id " +
                " INNER JOIN chip c ON c.id = cli.chip_id WHERE 1=1 ");

        if (regionalOriginIds != null && regionalOriginIds.size() > 0) {
            queryString.append(" AND cl.regional_origin_id IN :regionalOriginIds ");
            queryObject.putParameter("regionalOriginIds", regionalOriginIds);
        }

        if (regionalDestinationIds != null && regionalDestinationIds.size() > 0) {
            queryString.append(" AND cl.regional_destination_id IN :regionalDestinationIds ");
            queryObject.putParameter("regionalDestinationIds", regionalDestinationIds);
        }

        if (StringUtils.hasText(searchText)) {
            queryString.append(" AND c.icc_id LIKE :searchText ");
            queryObject.putParameter("searchText", "%" + searchText + "%");
        }

        if (lateOnly != null && lateOnly) {
            queryString.append(" AND cl.expected_arrival_date < :today AND cl.status = :status ");
            queryObject.putParameter("today", DateUtil.getAtMiddayTime(new Date()));
            queryObject.putParameter("status", ChipLotStatusType.WAITING.getCode());
        }

        queryObject.setQueryString(queryString.toString());
        return queryObject;
    }


}
