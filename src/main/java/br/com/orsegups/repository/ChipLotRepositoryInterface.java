package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.ChipLot;
import br.com.orsegups.entity.RegionalOffice;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChipLotRepositoryInterface {

    PageDTO<ChipLot> findListToReceiveByFilters(String searchText, List<Long> regionalDestinationIds, Boolean lateOnly, Pageable pageable);

    PageDTO<ChipLot> findSentListByFilters(String searchText, List<Long> regionalOriginIds, List<Long> regionalDestinationIds, Boolean lateOnly, Pageable pageable);

}
