package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.Chip;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ChipRepositoryInterface {

    PageDTO<Chip> findInStockByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

    PageDTO<Chip> findInTechnicianByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

    PageDTO<Chip> findInCustomerByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

    PageDTO<Chip> findInEquipmentByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

    PageDTO<Chip> findToCancelByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

    PageDTO<Chip> findCanceledByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

}
