package br.com.orsegups.repository;

import br.com.orsegups.entity.CustomerLink;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerLinkRepository extends JpaRepository<CustomerLink, Long>, CustomerLinkRepositoryInterface {

    List<CustomerLink> findByRemovalDateIsNullAndCompanyIdAndCentralId(Long companyId, String centralId);

}
