package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.CustomerLink;
import br.com.orsegups.repository.utils.AbstractRepositoryImpl;
import br.com.orsegups.repository.utils.QueryObject;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Component
public class CustomerLinkRepositoryImpl extends AbstractRepositoryImpl<CustomerLink> implements CustomerLinkRepositoryInterface {

    public PageDTO<CustomerLink> findInstalledByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, regionalIds);
        return executePagedQuery(queryObject, pageable, "cl");
    }

    private QueryObject getQueryObject(String searchText, List<Long> regionalIds) {
        QueryObject queryObject = new QueryObject();
        StringBuilder queryString = new StringBuilder(" FROM customer_link cl INNER JOIN chip c ON c.id = cl.chip_id " +
                " WHERE cl.removal_date IS NULL AND cl.regional_office_id IN :regionalIds ");
        queryObject.putParameter("regionalIds", regionalIds);

        if (StringUtils.hasText(searchText)) {
            queryString.append(" AND (c.icc_id LIKE :searchText ");
            queryString.append(" OR CONCAT(cl.sigma_central_id, '[', cl.sigma_partition, '] ', cl.sigma_business_name) LIKE :searchText) ");
            queryObject.putParameter("searchText", "%" + searchText + "%");
        }

        queryObject.setQueryString(queryString.toString());
        return queryObject;
    }


}
