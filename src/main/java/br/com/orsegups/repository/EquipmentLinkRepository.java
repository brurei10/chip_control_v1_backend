package br.com.orsegups.repository;

import br.com.orsegups.entity.EquipmentLink;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentLinkRepository extends JpaRepository<EquipmentLink, Long>, EquipmentLinkRepositoryInterface {


}
