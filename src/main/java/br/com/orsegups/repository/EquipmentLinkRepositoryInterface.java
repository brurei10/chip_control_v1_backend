package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.EquipmentLink;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EquipmentLinkRepositoryInterface {

    PageDTO<EquipmentLink> findLinkedByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

}
