package br.com.orsegups.repository;

import br.com.orsegups.entity.RegionalOffice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegionalOfficeRepository extends JpaRepository<RegionalOffice, Long> {

    RegionalOffice findByAcronym(String acronym);

}
