package br.com.orsegups.repository;

import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.TechnicianDelivery;
import br.com.orsegups.entity.TechnicianDeliveryChip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnicianDeliveryChipRepository extends JpaRepository<TechnicianDeliveryChip, Long> {

    TechnicianDeliveryChip findByChipAndTechnicianDelivery(Chip chip, TechnicianDelivery technicianDelivery);

}
