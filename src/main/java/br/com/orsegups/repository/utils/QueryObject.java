package br.com.orsegups.repository.utils;

import java.util.HashMap;
import java.util.Map;

public class QueryObject {
    private Map<String, Object> parameters;
    private String queryString;

    public QueryObject() {
        this.parameters = new HashMap<>();
    }

    public void putParameter(String key, Object value) {
        if (this.parameters == null)
            this.parameters = new HashMap<>();
        parameters.put(key, value);
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }
}