package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.entity.*;
import br.com.orsegups.entity.type.HistoryType;
import br.com.orsegups.repository.ChipHistoryRepository;
import br.com.orsegups.util.SessionUtil;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlService
public class ChipHistoryService {

    @Inject
    private ChipHistoryRepository repository;
    @Inject
    private SessionUtil sessionUtil;

    public List<ChipHistory> findByChipId(Long chipId) {
        return repository.findByChipId(chipId);
    }

    public void chipCreated(List<Chip> chips) {
        List<ChipHistory> chipHistories = chips.stream().map(chip ->
                new ChipHistory(chip, chip.getCreatedDate(), HistoryType.CHIP_CREATED)
        ).collect(Collectors.toList());

        saveAll(chipHistories);
    }
    public void chipCreated(Chip chip) {
        ChipHistory chipHistory = new ChipHistory(chip, chip.getCreatedDate(), HistoryType.CHIP_CREATED);
        save(chipHistory);
    }

    public void lotSent(List<Chip> chips, ChipLot chipLot) {
        List<ChipHistory> chipHistories = chips.stream().map(chip ->
                new ChipHistory(chip, chipLot.getCreatedDate(), chipLot, HistoryType.LOT_SENT)
        ).collect(Collectors.toList());

        saveAll(chipHistories);
    }

    public void lotCanceled(List<Chip> chips, ChipLot chipLot) {
        List<ChipHistory> chipHistories = chips.stream().map(chip ->
                new ChipHistory(chip, chipLot.getCancellationDate(), chipLot, HistoryType.LOT_CANCELED)
        ).collect(Collectors.toList());

        saveAll(chipHistories);
    }

    public void lotReceived(List<Chip> chips, ChipLot chipLot) {
        List<ChipHistory> chipHistories = chips.stream().map(chip ->
                new ChipHistory(chip, chipLot.getArrivalDate(), chipLot, HistoryType.LOT_RECEIVED)
        ).collect(Collectors.toList());

        saveAll(chipHistories);
    }

    public void technicianDelivered(List<Chip> chips, TechnicianDelivery technicianDelivery) {
        List<ChipHistory> chipHistories = chips.stream().map(chip ->
                new ChipHistory(chip, technicianDelivery.getDeliveryDate(), technicianDelivery, HistoryType.TECHNICIAN_DELIVERED)
        ).collect(Collectors.toList());

        saveAll(chipHistories);
    }

    public void technicianReturned(Chip chip, TechnicianDelivery technicianDelivery) {
        ChipHistory chipHistory = new ChipHistory(chip, new Date(), technicianDelivery, HistoryType.TECHNICIAN_RETURNED);
        save(chipHistory);
    }

    public void customerLink(CustomerLink customerLink) {
        ChipHistory chipHistory = new ChipHistory(customerLink.getChip(), customerLink.getInstallationDate(), customerLink, HistoryType.CUSTOMER_LINK);
        save(chipHistory);
    }

    public void customerUnlink(CustomerLink customerLink) {
        ChipHistory chipHistory = new ChipHistory(customerLink.getChip(), customerLink.getRemovalDate(), customerLink, HistoryType.CUSTOMER_UNLINK);
        save(chipHistory);
    }

    public void equipmentLink(EquipmentLink equipmentLink) {
        ChipHistory chipHistory = new ChipHistory(equipmentLink.getChip(), equipmentLink.getInstallationDate(), equipmentLink, HistoryType.EQUIPMENT_LINK);
        save(chipHistory);
    }

    public void equipmentUnlink(EquipmentLink equipmentLink) {
        ChipHistory chipHistory = new ChipHistory(equipmentLink.getChip(), equipmentLink.getRemovalDate(), equipmentLink, HistoryType.EQUIPMENT_UNLINK);
        save(chipHistory);
    }

    public void requestCancellation(Chip chip) {
        ChipHistory chipHistory = new ChipHistory(chip, chip.getRequestedCancellationDate(), chip.getCancellationReason(), HistoryType.REQUEST_CANCELLATION);
        chipHistory.setRegionalOffice(chip.getCanceledFromRegional());
        save(chipHistory);
    }

    public void canceled(Chip chip) {
        ChipHistory chipHistory = new ChipHistory(chip, chip.getCancellationDate(), HistoryType.CANCELED);
        save(chipHistory);
    }

    public void undoCancellation(Chip chip, String observation) {
        ChipHistory chipHistory = new ChipHistory(chip, new Date(), observation, HistoryType.UNDO_CANCELLATION);
        save(chipHistory);
    }

    private void setupLoggedUser(ChipHistory chipHistory) {
        chipHistory.setUser(sessionUtil.getCurrentUserLogin());
        if (sessionUtil.isCurrentUserTechnician())
            chipHistory.setUserType("SIGMA");
        else
            chipHistory.setUserType("FUSION");
    }

    private ChipHistory save(ChipHistory chipHistory) {
        setupLoggedUser(chipHistory);
        return repository.save(chipHistory);
    }

    private List<ChipHistory> saveAll(List<ChipHistory> chipHistories) {
        chipHistories.forEach(this::setupLoggedUser);
        return repository.saveAll(chipHistories);
    }
}
