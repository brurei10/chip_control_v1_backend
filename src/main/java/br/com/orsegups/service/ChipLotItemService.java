package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.ChipLotItem;
import br.com.orsegups.entity.type.ChipLotItemStatusType;
import br.com.orsegups.repository.ChipLotItemRepository;

@ChipControlService
public class ChipLotItemService extends AbstractService<ChipLotItem, ChipLotItemRepository> {

    public Integer countOpenLotWithChip(Chip chip) {
        return repository.countByStatusAndChip(ChipLotItemStatusType.WAITING, chip);
    }

}
