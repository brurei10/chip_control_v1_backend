package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.CompanyDTO;
import br.com.orsegups.entity.Company;
import br.com.orsegups.repository.CompanyRepository;

@ChipControlService
public class CompanyService extends AbstractService<Company, CompanyRepository> {

    public static final Long ORSEGUPS_ID = 859722591L;

    public Company getFromDTO(CompanyDTO companyDTO) {
        return findById(companyDTO.getId());
    }

    public Company findOrsegups() {
        return findById(ORSEGUPS_ID);
    }
}
