package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.CustomerLink;
import br.com.orsegups.repository.CustomerLinkRepository;
import org.springframework.data.domain.Pageable;

import java.util.List;

@ChipControlService
public class CustomerLinkService extends AbstractService<CustomerLink, CustomerLinkRepository> {

    public PageDTO<CustomerLink> findInstalledByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findInstalledByFilters(searchText, regionalIds, pageable);
    }

    public List<CustomerLink> findByRemovalDateIsNullAndCompanyIdAndCentralId(Long companyId, String centralId) {
        return repository.findByRemovalDateIsNullAndCompanyIdAndCentralId(companyId, centralId);
    }

}
