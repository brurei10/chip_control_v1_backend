package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.OperatorStatsDTO;
import br.com.orsegups.dto.RegionalStatsDTO;
import br.com.orsegups.repository.DashboardRepository;

import javax.inject.Inject;
import java.util.List;

@ChipControlService
public class DashboardService {

    @Inject
    private DashboardRepository repository;

    public List<RegionalStatsDTO> getOfficesStats() {
        return repository.getStatsGroupByRegional(1);
    }

    public List<RegionalStatsDTO> getEquipmentsStats() {
        return repository.getStatsGroupByRegional(0);
    }

    public List<OperatorStatsDTO> getActiveOperatorStats() {
        return repository.getActiveOperatorStats();
    }

    public List<RegionalStatsDTO> getStockStatsByRegional() {
        return repository.getStockStatsByRegional();
    }
    

}
