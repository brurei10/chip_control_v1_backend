package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.EquipmentLink;
import br.com.orsegups.repository.EquipmentLinkRepository;
import org.springframework.data.domain.Pageable;

import java.util.List;

@ChipControlService
public class EquipmentLinkService extends AbstractService<EquipmentLink, EquipmentLinkRepository> {

    public PageDTO<EquipmentLink> findLinkedByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findLinkedByFilters(searchText, regionalIds, pageable);
    }

}
