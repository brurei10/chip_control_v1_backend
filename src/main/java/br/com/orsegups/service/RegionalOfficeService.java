package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.RegionalOfficeDTO;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.repository.RegionalOfficeRepository;
import br.com.orsegups.util.SessionUtil;
import br.com.orsegups.validator.RegionalOfficeValidator;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

@ChipControlService
public class RegionalOfficeService extends AbstractService<RegionalOffice, RegionalOfficeRepository> {

    public static final Long STOCK_TI_ID = 870182283L;
    public static final Long STOCK_TO_CANCEL_ID = 931953043L;
    public static final Long TRACKING_ID = 870183722L;
    public static final Long DEGGY_ID = 917604768L;
    public static final Long NEXTI_ID = 794740692L;
    public static final Long ORSEGUPS_PRIME_ID = 1077074378L;
    public static final Long EXECUTIVES_PRIME_ID = 1080163557L;

    @Inject
    private SessionUtil sessionUtil;
    @Inject
    private RegionalOfficeValidator regionalOfficeValidator;

    public RegionalOffice findByAcronym(String acronym) {
        return repository.findByAcronym(acronym);
    }

    public RegionalOffice findStockTI() {
        return findById(STOCK_TI_ID);
    }

    public RegionalOffice findStockToCancel() {
        return findById(STOCK_TO_CANCEL_ID);
    }

    public RegionalOffice findOrsegupsPrime() {
        return findById(ORSEGUPS_PRIME_ID);
    }

    public RegionalOffice getFromDTO(RegionalOfficeDTO regionalOfficeDTO) {
        return findById(regionalOfficeDTO.getId());
    }

    public List<Long> getRegionalIdsFilteredByPermissions(RegionalOfficeDTO regionalDTO) {
        if (regionalDTO != null) {
            RegionalOffice regional = findById(regionalDTO.getId());
            if (sessionUtil.isCurrentUserAdmin()) {
                return Collections.singletonList(regional.getId());
            } else {
                regionalOfficeValidator.validateUserInRegional(regional);
                return Collections.singletonList(regional.getId());
            }
        } else {
            return sessionUtil.isCurrentUserAdmin() ? null : sessionUtil.getCurrentUserRegionalIds();
        }
    }

}
