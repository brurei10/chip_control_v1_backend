package br.com.orsegups.service.utils;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.entity.*;
import br.com.orsegups.entity.type.TechnicianDeliveryStatusType;
import br.com.orsegups.service.*;
import br.com.orsegups.validator.ChipValidator;
import br.com.orsegups.validator.CustomerLinkValidator;
import br.com.orsegups.validator.EquipmentLinkValidator;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlService
public class CancelUtilService {

    @Inject
    private RegionalOfficeService regionalOfficeService;
    @Inject
    private CustomerLinkService customerLinkService;
    @Inject
    private EquipmentLinkService equipmentLinkService;
    @Inject
    private TechnicianDeliveryChipService technicianDeliveryChipService;
    @Inject
    private ChipService chipService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private ChipValidator chipValidator;
    @Inject
    private CustomerLinkValidator customerLinkValidator;
    @Inject
    private EquipmentLinkValidator equipmentLinkValidator;

    public void requestChipCancellationByCustomer(List<CustomerLink> customerLinks, String reason) {
        customerLinks.forEach(customerLink -> requestChipCancellation(customerLink.getChip(), reason));
    }

    public Chip requestChipCancellation(Chip chip, String reason) {
        RegionalOffice toCancelRegional = regionalOfficeService.findStockToCancel();

        if (chip.getCustomerLink() != null)
            cancelCustomerLink(chip.getCustomerLink(), reason);
        if (chip.getEquipmentLink() != null)
            cancelEquipmentLink(chip.getEquipmentLink(), reason);

        return requestChipCancellation(chip, toCancelRegional, reason);
    }
    public List<Chip> requestChipCancellation(List<Chip> chips, String reason) {
    	return chips.stream().map(chip -> requestChipCancellation(chip, reason)).collect(Collectors.toList());
    }
    public Chip undoCancellation(Chip chip, String reason) {
        chip.setRegionalOffice(chip.getCanceledFromRegional());
        chip.setCanceledFromRegional(null);
        chip.setCancellationReason(null);
        chip.setRequestedCancellationDate(null);
        chip = chipService.save(chip);
        chipHistoryService.undoCancellation(chip, reason);
        return chip;
    }

    private Chip requestChipCancellation(Chip chip, RegionalOffice toCancelRegional, String reason) {
    	chipValidator.validateCancellation(chip);
        chip.setCanceledFromRegional(chip.getRegionalOffice());
        chip.setRegionalOffice(toCancelRegional);
        chip.setCustomerLink(null);
        chip.setTechnicianDelivery(null);
        chip.setEquipmentLink(null);
        chip.setCancellationReason(reason);
        chip.setRequestedCancellationDate(new Date());
        chip = chipService.save(chip);
        chipHistoryService.requestCancellation(chip);
        return chip;
    }

    private void cancelCustomerLink(CustomerLink customerLink, String reason) {
        if (customerLink.getRemovalDate() == null) {
            customerLinkValidator.validateCancel(customerLink);
            customerLink.setRemovalDate(new Date());
            customerLink.setRemovalReason(reason);
            customerLinkService.save(customerLink);
            chipHistoryService.customerUnlink(customerLink);
        }
    }

    private void cancelEquipmentLink(EquipmentLink equipmentLink, String reason) {
        if (equipmentLink.getRemovalDate() == null) {
            equipmentLinkValidator.validateCancel(equipmentLink);
            equipmentLink.setRemovalDate(new Date());
            equipmentLink.setRemovalReason(reason);
            equipmentLinkService.save(equipmentLink);
            chipHistoryService.equipmentUnlink(equipmentLink);
        }
    }

    public void cancelTechnicianDeliveryChip(TechnicianDeliveryChip technicianDeliveryChip) {
        technicianDeliveryChip.setStatus(TechnicianDeliveryStatusType.RETURNED);
        technicianDeliveryChipService.save(technicianDeliveryChip);
        chipHistoryService.technicianReturned(technicianDeliveryChip.getChip(), technicianDeliveryChip.getTechnicianDelivery());
    }

    public List<Chip> cancel(List<Chip> chips) {
        return chips.stream().map(this::cancel).collect(Collectors.toList());
    }

    public Chip cancel(Chip chip) {
        chipValidator.validateCancel(chip);
        chip.setCancellationDate(new Date());
        chip.setRegionalOffice(null);
        chip = chipService.save(chip);
        chipHistoryService.canceled(chip);
        return chip;
    }
}
