package br.com.orsegups.util.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReportFactory {

	public static ByteArrayInputStream createExcel(ReportModel reportModel) throws Exception {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        Row headerRow = sheet.createRow(0);

        //Create cells for header and set column format
		reportModel.getHeader().forEach(header -> {
			Cell cell = headerRow.createCell(header.getColumn());
			cell.setCellValue(header.getValue());

			CellStyle cellStyle;
			switch (header.getType()) {
				case CellHelper.FORMAT_DATE:
					cellStyle = CellHelper.getDateStyle(workbook);
					break;
				case CellHelper.FORMAT_TEXT:
				default:
					cellStyle = CellHelper.getTextStyle(workbook);
			}
			((XSSFSheet)sheet).getColumnHelper().setColDefaultStyle(header.getColumn(), cellStyle);

		});

        //Create cells
        for (int i = 0; i < reportModel.getRows().size(); i++) {
        	Row row = sheet.createRow(i+1);
        	
        	for (int j = 0; j < reportModel.getRows().get(i).size(); j++) {
        		Cell cell = row.createCell(j);
        		CellHelper.setCellValue(cell, reportModel.getRows().get(i).get(j));
				cell.setCellStyle(sheet.getColumnStyle(j));
        	}
		}
        //Resize all columns to fit the content size
        for(int i = 0; i < reportModel.getHeader().size(); i++) {
            sheet.autoSizeColumn(i);
        }
        
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			workbook.write(out);
			workbook.close();
			return new ByteArrayInputStream(out.toByteArray());
		    
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
