package br.com.orsegups.util.report;


public class ReportHeader {
	private Integer column;
	private String value;
	private String type;

	public ReportHeader() {
		super();
	}

	public ReportHeader(Integer column, String value, String type) {
		this();
		this.column = column;
		this.value = value;
		this.type = type;
	}

	public Integer getColumn() {
		return column;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
