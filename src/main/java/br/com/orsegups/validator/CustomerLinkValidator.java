package br.com.orsegups.validator;

import br.com.orsegups.entity.CustomerLink;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.util.SessionUtil;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class CustomerLinkValidator extends Validator {

    @Inject
    private SessionUtil sessionUtil;

    public void validateCancel(CustomerLink customerLink) {
        if (sessionUtil.isCurrentUserRegionalManager()) {
            List<RegionalOffice> userRegionals = sessionUtil.getCurrentUserRegional();
            if (!userRegionals.contains(customerLink.getRegionalOffice())) {
                throw new CustomException(messages.get("validate.customerLink.cancel.notInRegional", customerLink.getRegionalOffice().getName()));
            }
        }
    }

}
